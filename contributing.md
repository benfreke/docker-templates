# Contributing

All contributions must have tests (located in the `tests` directory).
Since all templates and files have tests there should be some easy to follow examples.

## Updating versions

> **_NOTE:_** You will need to add the injection of _new_ versions into `docker-compose-test.yml` and `phpunit.xml`.

PHP is installed as the base Docker image that all work is based on.

The latest versions can be found on the [official PHP releases page](https://www.php.net/downloads.php).

Version numbers for
[PHP](https://www.php.net/downloads.php),
[Composer](https://getcomposer.org/),
[XDebug](https://xdebug.org/download),
[Nginx](http://nginx.org/en/download.html),
[Redis](https://hub.docker.com/_/redis),
[MySQL](https://dev.mysql.com/downloads/mysql/),
[Postgres](https://www.postgresql.org/)
versions are all stored in environment values.

Locations to update when changing a version number are:

1. `tests/env-local-build.env`.
   This contains the version number that is used when running the local tests (not PHPUnit).
2. `.gitlab-ci.yml`.
   This contains the PHP (only) version numbers that is used in CI/CD.
3. `README.md`.
   This is the public facing documentation.

## How to simulate the CI/CD pipline locally

1. Make sure [Docker](https://docs.docker.com/) is installed on your machine.
1. Run each of the `test-php*.sh` files in the `/tests` directory.
    1. These mimic the Gitlab pipeline locally to allow faster feedback of changes.
    1. These must be run from the `tests` directory.

## Building a release

This is a manual process, due to the difficulty of using the tag number in an automated fashion.
There is an assumption that all work happens in a branch related to the version that is being released.

1. Create a `Merge Request` for the branch with the changes for this release.
    1. This step is optional if there are multiple feature branches to be merged to `main`.
2. Set the Milestone for the Merge Request to be the release.
3. Merge the branch to the `main` branch.
    1. Manually adjust the commit message to include all features.
    2. This is because we squash commits and delete the source branch.
4. In the `issues` section of Gitlab, ensure all issues for this version are tagged against the same milestone as the
   version.
    1. e.g. The `Milestone` could be `1.2.1`, in which case the tag would also be `1.2.1`.
5. Close all these issues for this `Milestone`.
6. Go to `Releases`.
7. Create a new `Tag`.
    1. This should be a lightweight tag.
8. Add the `Milestone` to this `Release`.
9. Manually create the release notes.
    1. Copy these from the commit messages.
    2. Go to your `Pipelines`.
    3. Find the pipeline for the `Merge Request` you just created.
    4. Click on this.
    5. Click on the short hash for the commit.
    6. This will have the notes you created in the squashed commits.
    7. Ensure you always add the PHP versions from the `Readme.md` file as well.
10. Add the URL for the stored artifacts.
    1. Go back to the Pipeline for the Merge Request.
    2. Find the `Store:Artifacts` job and click on it.
    1. Click the `Browse` button.
    1. Click on the `public` folder.
    2. Copy and paste the page URL to the `URL` field in the `Release` we're creating. 
11. Click the `Create release` button.
12. Head to the main page of the Repository, and check that the `Latest Release` button points to where we want it to.
13. Add a new `Milestone` for the next release on the `Milestones` page in Gitlab.
    1. If required, add a Major, Minor and Patch version.
14. Close the old `Milestone`.
   

## Where files live

1. Dockerfile templates are in `/src/Dockerfile/Templates`.
2. Dockerfile build files are in `/src/Dockerfile/Files`.
3. Docker Compose templates are in `/src/Dockercompose/Templates`.
4. Docker Compose build files are in `/src/Dockercompose/Files`.
5. The Nginx conf file is in `/scripts/nginx/conf.d`.
6. The .env.* templates are in `/src/env/Templates`.
7. The .env.* build files are in `/src/env/Files`.
8. The build process is all documented (poorly) in `.gitlab-ci.yml`
