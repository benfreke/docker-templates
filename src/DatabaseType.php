<?php

declare(strict_types=1);

namespace App;

/**
 * Replace with an enum when 8.1 is out
 */
class DatabaseType
{
    public const TYPE_NONE = 1;

    public const TYPE_MYSQL = 2;

    public const TYPE_PGSQL = 3;

    protected int $dbType;

    public function __construct(int $dbType)
    {
        $this->dbType = match ($dbType) {
            self::TYPE_PGSQL => self::TYPE_PGSQL,
            self::TYPE_MYSQL => self::TYPE_MYSQL,
            default => self::TYPE_NONE,
        };
    }

    public function getType(): int
    {
        return $this->dbType;
    }
}
