<?php

declare(strict_types=1);

namespace App\Dockerfile\Files;

use App\BuildFileInterface;
use App\Dockerfile\Templates\AbstractTemplate;
use App\Dockerfile\Templates\Php as PhpTemplate;
use Exception;
use Override;

/**
 * Class AbstractBuild
 *
 * @package App\Dockerfile\Files
 */
class Php implements BuildFileInterface
{
    protected const FILENAME = 'Dockerfile';

    protected string $fileContents = '';

    public function __construct(
        protected AbstractTemplate $classToUse,
        protected AbstractTemplate $developmentClassToUse,
        protected AbstractTemplate|null $databaseClassToUse,
    ) {}

    /**
     *
     * @throws Exception
     */
    #[Override]
    public function run(
        string $exportDirectory,
        string $fileName = self::FILENAME,
    ): void {
        $this->addSection(
            (new PhpTemplate())->getValue(
                [
                    getenv('PHP_BUILD_VERSION'),
                    $this->getShortVersion(),
                    $this->getFilepathVersion(),
                    getenv('COMPOSER_VERSION'),
                    $this->classToUse->getValue(),
                    $this->databaseClassToUse?->getValue(),
                ],
                [
                    '%%PHP_BUILD_VERSION%%',
                    '%%PHP_SHORT_VERSION%%',
                    '%%PHP_FILEPATH_VERSION%%',
                    '%%COMPOSER_VERSION%%',
                    '%%OS_REQUIREMENTS%%',
                    '%%DATABASE_REQUIREMENTS%%',
                ],
            ),
        );
        $this->addSection(
            $this->developmentClassToUse->getValue(
                [
                    getenv('XDEBUG_VERSION'),
                ],
                [
                    '%%XDEBUG_VERSION%%',
                ],
            ),
        );

        file_put_contents($exportDirectory . $fileName, $this->fileContents);
    }

    /**
     * Adds content to the file
     */
    public function addSection(string $newSection): void
    {
        $this->fileContents .= $newSection . "\n";
    }

    /**
     * Returns the filepath version of the full PHP Version
     *
     * @throws Exception
     */
    protected function getFilepathVersion(): string
    {
        $version = getenv('PHP_BUILD_VERSION');
        if (empty($version)) {
            throw new Exception('PHP_BUILD_VERSION not set');
        }
        $versionNumbers = explode('.', $version);
        return $versionNumbers[0] . '.' . $versionNumbers[1];
    }

    /**
     * Remove the dots from the filepath version
     *
     * @throws Exception
     */
    protected function getShortVersion(): string
    {
        return str_replace('.', '', $this->getFilepathVersion());
    }
}
