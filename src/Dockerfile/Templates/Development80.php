<?php

declare(strict_types=1);

namespace App\Dockerfile\Templates;

/**
 * The multi-stage development section
 *
 * This installs xdebug and some handy scripts for local development
 * - File Compare: Ensures you include all your keys in all your .env files
 */
class Development80 extends AbstractTemplate
{
    public function __construct()
    {
        $template = <<<'TEMPLATE'
# This is my development image. We add xdebug because it is awesome
FROM application-base AS development

# We need to install OS packages as the root user
USER root

# PECL needs this
RUN apk add --no-cache linux-headers=~5.16

# Install xdebug via pecl. In theory we could install from sources, but this is easier
RUN pecl install xdebug-%%XDEBUG_VERSION%% \
    && rm -rf /tmp/pear \
    && docker-php-ext-enable xdebug

# Copy xdebug config and ensure we can use it
COPY xdebug.ini /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

RUN chmod 644 /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    # sed takes a backup, so we need permissions on the folder as well
    && chmod 755 /usr/local/etc/php/conf.d

# The entrypoint will set up xdebug based on the flag and ensure the we have the right number of values in .env files
# It also checks that we have the same keys between .env and .env.example
COPY entrypoint.sh /usr/local/bin/entrypoint.sh
COPY file-compare.php /usr/local/bin/file-compare.php
COPY CompareIniFiles.php /usr/local/bin/CompareIniFiles.php
RUN chmod 664 /usr/local/bin/entrypoint.sh \
    && chmod +x /usr/local/bin/entrypoint.sh \
    && chmod 664 /usr/local/bin/file-compare.php \
    && chmod +x /usr/local/bin/file-compare.php \
    && chmod 664 /usr/local/bin/CompareIniFiles.php

# Set an entrypoint which handles some startup logic
ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]

# Set the working directory for script purposes
WORKDIR /var/www/html

# Set the user to not be root now we've installed everything
USER appuser

TEMPLATE;
        $this->setTemplate($template);
    }
}
