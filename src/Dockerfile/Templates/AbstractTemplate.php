<?php

declare(strict_types=1);

namespace App\Dockerfile\Templates;

use App\TemplateInterface;
use Override;

/**
 * Class AbstractTemplate
 *
 * @package App\Dockerfile\Templates
 */
abstract class AbstractTemplate implements TemplateInterface
{
    protected const DEFAULT_SEARCH_STRING = '%%%%';

    protected string $template = '';

    public function setTemplate(string $newTemplate): AbstractTemplate
    {
        $this->template = $newTemplate;
        return $this;
    }


    #[Override]
    public function getValue(array $replace = [null], ?array $search = null): string
    {
        // Allow resetting search template
        if ($search === null || $search === []) {
            $search = [self::DEFAULT_SEARCH_STRING];
        }

        return str_replace($search, $replace, $this->getTemplate());
    }

    protected function getTemplate(): string
    {
        return $this->template;
    }
}
