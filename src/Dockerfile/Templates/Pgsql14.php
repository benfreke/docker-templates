<?php

declare(strict_types=1);

namespace App\Dockerfile\Templates;

/**
 * pgsql template for a Dockerfile
 */
class Pgsql14 extends AbstractTemplate
{
    public function __construct()
    {
        $template = <<<'TEMPLATE'
# Now install pgsql
RUN apk add --no-cache --virtual .build-pgsql-deps \
            postgresql14-dev=~14.12 \
        && apk add --no-cache --virtual .required-pgsql-deps \
            postgresql14=~14.12 \
    && NPROC=$(grep -c ^processor /proc/cpuinfo 2>/dev/null || 1) \
    && docker-php-ext-install -j"${NPROC}" \
        pdo \
        pdo_pgsql \
        pgsql

TEMPLATE;
        $this->setTemplate($template);
    }
}
