<?php

declare(strict_types=1);

namespace App\Dockerfile\Templates;

/**
 *
 */
class Mysql extends AbstractTemplate
{
    public function __construct()
    {
        $template = <<<'TEMPLATE'
# Now install mysql
RUN NPROC=$(grep -c ^processor /proc/cpuinfo 2>/dev/null || 1) \
    && docker-php-ext-install -j"${NPROC}" \
        pdo_mysql

TEMPLATE;
        $this->setTemplate($template);
    }
}
