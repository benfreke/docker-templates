<?php

declare(strict_types=1);

namespace App\Dockerfile\Templates;

/**
 * Class PhpOsInstall
 *
 * @package App\Dockerfile\Templates
 */
class Php80OsInstall extends AbstractTemplate
{
    public function __construct()
    {
        $template = <<<'TEMPLATE'
RUN apk add --no-cache --virtual .build-deps \
        autoconf=~2.71 \
        freetype-dev=~2.12 \
        libjpeg-turbo-dev=~2.1 \
        libpng-dev=~1.6 \
        oniguruma-dev=~6.9 \
        icu-dev=~71.1 \
    # We need PECL for phpredis and xdebug
    && apk add --no-cache --virtual .pecl-deps \
        coreutils=~9.1 \
        g++=~11.2 \
        gcc=~11.2 \
        git=~2.36 \
        make=~4.3 \
        libzip-dev=~1.8 \
    && apk add --no-cache --virtual .required-deps \
        freetype=~2.12 \
        libjpeg-turbo=~2.1 \
        libpng=~1.6 \
        icu-libs=~71.1 \
    && docker-php-ext-configure gd \
        --with-freetype \
        --with-jpeg \
    && NPROC=$(grep -c ^processor /proc/cpuinfo 2>/dev/null || 1) \
    && docker-php-ext-install -j"${NPROC}" \
        gd \
        opcache \
        intl \
        mbstring \
        tokenizer \
        zip

TEMPLATE;
        $this->setTemplate($template);
    }
}
