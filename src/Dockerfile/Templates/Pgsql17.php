<?php

declare(strict_types=1);

namespace App\Dockerfile\Templates;

/**
 * pgsql template for a Dockerfile
 */
class Pgsql17 extends AbstractTemplate
{
    public function __construct()
    {
        $template = <<<'TEMPLATE'
# Now install pgsql
RUN apk add --no-cache --virtual .build-pgsql-deps \
            postgresql17-dev=~17.2 \
        && apk add --no-cache --virtual .required-pgsql-deps \
            postgresql17=~17.2 \
    && NPROC=$(grep -c ^processor /proc/cpuinfo 2>/dev/null || 1) \
    && docker-php-ext-install -j"${NPROC}" \
        pdo \
        pdo_pgsql \
        pgsql

TEMPLATE;
        $this->setTemplate($template);
    }
}
