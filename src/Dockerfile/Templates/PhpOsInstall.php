<?php

declare(strict_types=1);

namespace App\Dockerfile\Templates;

/**
 * Class PhpOsInstall
 *
 * @package App\Dockerfile\Templates
 */
class PhpOsInstall extends AbstractTemplate
{
    public function __construct()
    {
        $template = <<<'TEMPLATE'
RUN apk add --no-cache --virtual .build-deps \
        autoconf=~2.72 \
        freetype-dev=~2.13 \
        libjpeg-turbo-dev=~3.0 \
        libpng-dev=~1.6 \
        oniguruma-dev=~6.9 \
        icu-dev=~74.2 \
    # We need PECL for phpredis and xdebug
    && apk add --no-cache --virtual .pecl-deps \
        coreutils=~9.5 \
        g++=~14.2 \
        gcc=~14.2 \
        git=~2.47 \
        make=~4.4 \
        libzip-dev=~1.11 \
    && apk add --no-cache --virtual .required-deps \
        freetype=~2.13 \
        libjpeg-turbo=~3.0 \
        libpng=~1.6 \
        icu-libs=~74.2 \
    && docker-php-ext-configure gd \
        --with-freetype \
        --with-jpeg \
    && NPROC=$(grep -c ^processor /proc/cpuinfo 2>/dev/null || 1) \
    && docker-php-ext-install -j"${NPROC}" \
        gd \
        opcache \
        intl \
        mbstring \
        zip

TEMPLATE;
        $this->setTemplate($template);
    }
}
