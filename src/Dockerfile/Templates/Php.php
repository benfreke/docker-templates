<?php

declare(strict_types=1);

namespace App\Dockerfile\Templates;

/**
 *
 */
class Php extends AbstractTemplate
{
    public function __construct()
    {
        $template = <<<'TEMPLATE'
# syntax=docker/dockerfile:1
# check=error=true

# This stage is my base PHP image. It does not have composer, etc, as they are not needed for all PHP
FROM php:%%PHP_BUILD_VERSION%%-fpm-alpine AS application-base

# Set the metadata
LABEL maintainer="Ben Freke (https://gitlab.com/benfreke)"

# Add some extensions that we are always going to need
# Install into alpine what we need to support the install
# This is split into build dependencies, and the libraries that actually required
# `docker-php-ext-configure` to use the php docker tools to configure GD
# `--no-cache` to remove cache folders
%%OS_REQUIREMENTS%%
# Install Redis
RUN pecl install -o -f redis \
    && rm -rf /tmp/pear \
    && docker-php-ext-enable \
        redis

%%DATABASE_REQUIREMENTS%%

# Get a working php-fpm
# This is correct for PHP%%PHP_SHORT_VERSION%% only
COPY php-fpm-%%PHP_SHORT_VERSION%%.conf /etc/php/%%PHP_FILEPATH_VERSION%%/fpm/php-fpm.conf

# Set the home directory for composer to use after installation
ENV COMPOSER_HOME=/tmp

# https://unix.stackexchange.com/questions/71622/
RUN chmod 1777 /tmp

# Set the version of composer that is installed
ENV COMPOSER_ROOT_VERSION=%%COMPOSER_VERSION%%

# Install composer
COPY --from=composer/composer:%%COMPOSER_VERSION%%-bin /composer /usr/bin/composer

# Create a group and user so we're not running as root always
RUN addgroup -S appgroup && adduser -S appuser -G appgroup

# Tell docker that all future commands should run as the appuser we just created
USER appuser

TEMPLATE;
        $this->setTemplate($template);
    }
}
