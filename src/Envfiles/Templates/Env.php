<?php

declare(strict_types=1);

namespace App\Envfiles\Templates;

/**
 * Template for all .env files
 */
class Env
{
    protected const DEFAULT_SEARCH_STRING = '%%%%';

    protected string $template = '';

    public function __construct()
    {
        $template = <<<'TEMPLATE'
APP_NAME=Laravel
APP_ENV=local
APP_KEY=
APP_DEBUG=true
APP_URL=http://localhost

LOG_CHANNEL=stack
LOG_LEVEL=debug

DB_CONNECTION=%%DB_CONNECTION%%
DB_HOST=%%DB_HOST%%
DB_PORT=%%DB_PORT%%
DB_DATABASE=%%DB_DATABASE%%
DB_USERNAME=%%DB_USERNAME%%
DB_PASSWORD=%%DB_PASSWORD%%

BROADCAST_DRIVER=log
CACHE_DRIVER=redis
FILESYSTEM_DRIVER=local
QUEUE_CONNECTION=redis
SESSION_DRIVER=redis
SESSION_LIFETIME=120

MEMCACHED_HOST=127.0.0.1

REDIS_HOST=redis
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_MAILER=smtp
MAIL_HOST=mailhog
MAIL_PORT=1025
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
MAIL_FROM_ADDRESS=null
MAIL_FROM_NAME="${APP_NAME}"

AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
AWS_DEFAULT_REGION=us-east-1
AWS_BUCKET=
AWS_USE_PATH_STYLE_ENDPOINT=false

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_APP_CLUSTER=mt1

MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"

COMPARE_ENV_FILES=1

# Set this to 1 to have Xdebug enabled when a container starts
XDEBUG_ENABLED=0

# Enable xdebug to work from the CLI as well
XDEBUG_CONFIG=idekey=DOCKER

# This is for your IDE, and tells it which server to look for
PHP_IDE_CONFIG=serverName=_

TEMPLATE;
        $this->setTemplate($template);
    }


    public function getValue(array $replace = [null], ?array $search = null): string
    {
        // Allow resetting search template
        if ($search === null || $search === []) {
            $search = [self::DEFAULT_SEARCH_STRING];
        }

        return str_replace($search, $replace, $this->getTemplate());
    }

    public function setTemplate(string $newTemplate): self
    {
        $this->template = $newTemplate;
        return $this;
    }

    public function getTemplate(): string
    {
        return $this->template;
    }
}
