<?php

declare(strict_types=1);

namespace App\Envfiles\Files;

use App\DatabaseType;
use App\Envfiles\Templates\Env as EnvTemplate;

/**
 *
 */
class Env
{
    private array $search = [
        '%%DB_CONNECTION%%',
        '%%DB_HOST%%',
        '%%DB_PORT%%',
        '%%DB_DATABASE%%',
        '%%DB_USERNAME%%',
        '%%DB_PASSWORD%%',
    ];

    public function __construct(
        private readonly string $exportDirectory,
        private readonly DatabaseType $databaseType,
        private readonly string $fileName = '.env',
        private readonly bool $testingFile = false,
    ) {}

    /**
     * Replace the values in the template string
     */
    public function run(): void
    {
        $replace = match ($this->databaseType->getType()) {
            DatabaseType::TYPE_MYSQL => $this->mysqlReplaceValues(),
            DatabaseType::TYPE_PGSQL => $this->pgsqlReplaceValues(),
            default => $this->noDbReplaceValues(),
        };

        // Dodgy hack to change the host name for testing
        // Except when we have no database, as it looks silly and confusing doing that
        if (
            $this->testingFile
            && $this->databaseType->getType() !== DatabaseType::TYPE_NONE
        ) {
            $replace[1] .= '-testing';
        }
        $fileContents = (new EnvTemplate())->getValue(
            $replace,
            $this->search,
        );
        file_put_contents($this->exportDirectory . DIRECTORY_SEPARATOR . $this->fileName, $fileContents);
    }

    private function mysqlReplaceValues(): array
    {
        return [
            'mysql',
            'mysql',
            '3306',
            'laravel',
            'laravel',
            'password',
        ];
    }

    private function noDbReplaceValues(): array
    {
        $result = $this->mysqlReplaceValues();
        $result[1] = '127.0.0.1';
        return $result;
    }

    private function pgsqlReplaceValues(): array
    {
        return [
            'pgsql',
            'pgsql',
            '5432',
            'laravel',
            'laravel',
            'password',
        ];
    }
}
