<?php

declare(strict_types=1);

namespace App;

/**
 * Interface for all template classes
 */
interface TemplateInterface
{
    /**
     * @param array<string>|array<float>|array<int>|array<null> $replace
     * @param array<string>|array<float>|array<int>|array<null> $search
     */
    public function getValue(array $replace, array $search): string | array;
}
