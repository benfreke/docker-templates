<?php

declare(strict_types=1);

namespace App;

/**
 *
 */
interface BuildFileInterface
{
    public function run(string $exportDirectory, string $fileName): void;
}
