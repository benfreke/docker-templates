<?php

declare(strict_types=1);

namespace App\Dockercompose\Templates;

/**
 *
 */
class Mysql extends AbstractTemplate
{
    protected const DATABASE_DATA_DIRECTORY = '/var/lib/mysql';

    public function __construct()
    {
        $this->setTemplate([
            '%%%%' => [
                'image' => 'mysql:%%%%',
                'init' => true,
                'environment' => [
                    'MYSQL_ROOT_PASSWORD=${DB_PASSWORD}',
                    'MYSQL_DATABASE=${DB_DATABASE}',
                    'MYSQL_PASSWORD=${DB_PASSWORD}',
                    'MYSQL_USER=${DB_USERNAME}',
                    'MYSQL_ALLOW_EMPTY_PASSWORD="yes"',
                ],
                'command' => '--default-authentication-plugin=mysql_native_password',
            ],
        ]);
    }
}
