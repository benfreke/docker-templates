<?php

declare(strict_types=1);

namespace App\Dockercompose\Templates;

/**
 *
 */
class Nginx extends AbstractTemplate
{
    public function __construct()
    {
        $this->setTemplate([
            'nginx' => [
                'image' => 'nginx:%%%%-alpine',
                'restart' => 'on-failure',
                'init' => true,
                'ports' => ['80:80'],
                'volumes' => [
                    './:/var/www/html:cached',
                    './scripts/nginx/conf.d/:/etc/nginx/conf.d/:cached',
                ],
            ],
        ]);
    }
}
