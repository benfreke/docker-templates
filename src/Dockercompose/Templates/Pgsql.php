<?php

declare(strict_types=1);

namespace App\Dockercompose\Templates;

/**
 *
 */
class Pgsql extends AbstractTemplate
{
    protected const DATABASE_DATA_DIRECTORY = '/var/lib/postgresql/data';

    public function __construct()
    {
        $this->setTemplate([
            '%%%%' => [
                'image' => 'postgres:%%%%-alpine',
                'init' => true,
                'environment' => [
                    'POSTGRES_DB=${DB_DATABASE}',
                    'POSTGRES_PASSWORD=${DB_PASSWORD}',
                    'POSTGRES_USER=${DB_USERNAME}',
                ],
            ],
        ]);
    }
}
