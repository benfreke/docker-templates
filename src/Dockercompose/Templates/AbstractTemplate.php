<?php

declare(strict_types=1);

namespace App\Dockercompose\Templates;

use App\Dockercompose\Files\AbstractBuild;
use App\TemplateInterface;
use Exception;
use InvalidArgumentException;
use Override;

/**
 * Class AbstractTemplate
 *
 * @package App\Dockercompose\Templates
 */
abstract class AbstractTemplate implements TemplateInterface
{
    public const DEFAULT_SEARCH_STRING = '%%%%';

    public const NETWORK_CACHE = 'redis';

    public const NETWORK_PHP = 'php';

    public const NETWORK_EXTERNAL = 'external';

    public const NETWORK_DATABASE = 'database';

    protected const DATABASE_DATA_DIRECTORY = '';

    protected array $template;


    public function setTemplate(array $newTemplate): AbstractTemplate
    {
        $this->template = $newTemplate;
        return $this;
    }

    /**
     * Change the name of the template. Used for databases
     */
    public function setTemplateName(
        string $newTemplateName,
        ?string $oldTemplateName = self::DEFAULT_SEARCH_STRING,
    ): AbstractTemplate {
        if ($newTemplateName === '' || $newTemplateName === '0') {
            throw new InvalidArgumentException('$newTemplateName cannot be empty');
        }

        $existingTemplate = $this->getTemplate();

        if (!isset($existingTemplate[$oldTemplateName])) {
            return $this;
        }

        $newTemplate = [];
        $newTemplate[$newTemplateName] = $existingTemplate[$oldTemplateName];

        return $this->setTemplate($newTemplate);
    }

    /**
     * @inheritDoc
     * @return array [string, array]
     */
    #[Override]
    public function getValue(
        array $replace = [null],
        ?array $search = null,
    ): array {
        if ($replace === [] || $replace[0] === null) {
            return $this->getTemplate();
        }

        // Allow resetting search template
        if ($search === null || $search === []) {
            $search = [self::DEFAULT_SEARCH_STRING];
        }

        $template = $this->getTemplate();

        foreach ($template as $key => $value) {
            $template[$key] = $this->safeReplacement($search, $replace, $value);
        }
        return $template;
    }

    /**
     * Adds a network to the service
     *
     *
     * @throws Exception
     */
    public function addNetwork(string $networkName): AbstractTemplate
    {
        $template = $this->getTemplate();
        $serviceName = array_key_first($template);

        if ($serviceName === 0 || ($serviceName === '' || $serviceName === '0') || $serviceName === null) {
            throw new Exception('Cannot get service from empty template');
        }

        // Only add the network once
        if (!$this->hasNetwork($serviceName, $networkName)) {
            $template[$serviceName]['networks'][] = $networkName;
        }

        return $this->setTemplate($template);
    }

    /**
     * @return array [string, array]
     */
    protected function getTemplate(): array
    {
        return $this->template;
    }

    /**
     * @param  string[]  $search
     * @param  string[]  $replace
     * @param  bool|string[]|string  $subject
     *
     * @return bool|string[]|string
     */
    protected function safeReplacement(
        array $search,
        array $replace,
        bool|array|string $subject,
    ): bool|array|string {
        // Always return booleans, as str_replace converts to a '1'
        if (is_bool($subject)) {
            return $subject;
        }

        // Recursively go over the array, and then return it
        if (is_array($subject)) {
            foreach ($subject as $subjectKey => $subjectValue) {
                $subject[$subjectKey] = $this->safeReplacement($search, $replace, $subjectValue);
            }
            return $subject;
        }

        // If it's just a string, str_replace is safe
        return str_replace($search, $replace, $subject);
    }

    protected function hasNetwork(string $serviceName, string $networkName): bool
    {
        if (!isset($this->getTemplate()[$serviceName]['networks'])) {
            return false;
        }
        return in_array($networkName, (array) $this->getTemplate()[$serviceName]['networks']);
    }

    public function addVolume(string $dbVolumeName = AbstractBuild::DATABASE_VOLUME_NAME): self
    {
        if ($dbVolumeName === '' || $dbVolumeName === '0') {
            return $this;
        }

        $oldTemplate = $this->getTemplate();
        if ($oldTemplate === []) {
            return $this;
        }

        $serviceKey = array_key_first($oldTemplate);
        $oldTemplate[$serviceKey]['volumes'] = ["$dbVolumeName:" . static::DATABASE_DATA_DIRECTORY];

        return $this->setTemplate($oldTemplate);
    }
}
