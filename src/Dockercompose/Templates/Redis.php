<?php

declare(strict_types=1);

namespace App\Dockercompose\Templates;

/**
 *
 */
class Redis extends AbstractTemplate
{
    /**
     * Redis constructor.
     */
    public function __construct()
    {
        $this->setTemplate([
            'redis' => [
                'image' => 'redis:%%%%-alpine',
                'init' => true,
            ],
        ]);
    }
}
