<?php

declare(strict_types=1);

namespace App\Dockercompose\Templates;

/**
 * Template for the PHP service
 */
class Php extends AbstractTemplate
{
    public function __construct()
    {
        $this->setTemplate([
            'php' => [
                'image' => 'benfreke/php:%%%%',
                'volumes' => [
                    '~/.composer:/tmp:delegated',
                    './:/var/www/html:delegated',
                ],
                'init' => true,
                'environment' => [
                    'COMPARE_ENV_FILES=${COMPARE_ENV_FILES:-0}',
                    'PHP_IDE_CONFIG=${PHP_IDE_CONFIG}',
                ],
            ],
        ]);
    }
}
