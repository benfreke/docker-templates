<?php

declare(strict_types=1);

namespace App\Dockercompose\Files;

use App\Dockercompose\Templates\AbstractTemplate;
use App\Dockercompose\Templates\Php as PhpTemplate;
use App\Dockercompose\Templates\Redis;
use Exception;
use Override;

/**
 * Class Php
 *
 * Builds a PHP docker-compose file with no Database connectivity
 *
 * @package App\Files
 */
class PhpCli extends AbstractBuild
{
    /**
     * @inheritDoc
     *
     * @throws Exception
     */
    #[Override]
    public function run(string $exportDirectory, string $fileName = self::FILENAME): void
    {
        // Set up the services and networks they're attached to

        $redis = new Redis();
        $php = new PhpTemplate();

        $php->addNetwork(AbstractTemplate::NETWORK_PHP)
            ->addNetwork(AbstractTemplate::NETWORK_CACHE);
        $redis->addNetwork(AbstractTemplate::NETWORK_CACHE);

        $this->addSection(
            [
                'networks' => [
                    AbstractTemplate::NETWORK_PHP => null,
                    AbstractTemplate::NETWORK_CACHE => null,
                ],
            ],
        );
        $this->addSection($php->getValue([$this->getEnvValue('PHP_BUILD_VERSION')]), 'services');
        $this->addSection($redis->getValue([$this->getEnvValue('REDIS_VERSION')]), 'services');


        file_put_contents($exportDirectory . $fileName, $this->export());
    }
}
