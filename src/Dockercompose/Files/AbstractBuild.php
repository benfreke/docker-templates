<?php

declare(strict_types=1);

namespace App\Dockercompose\Files;

use App\BuildFileInterface;
use Exception;
use Override;
use Symfony\Component\Yaml\Yaml;

/**
 * Abstract class for docker compose build files
 */
abstract class AbstractBuild implements BuildFileInterface
{
    protected const FILENAME = 'docker-compose.yml';

    public const DATABASE_VOLUME_NAME = 'db-data';

    /**
     * @var string[]
     */
    private array $sections = [];

    /**
     * Use this method to build the final file
     */
    #[Override]
    abstract public function run(string $exportDirectory, string $fileName): void;

    /**
     *
     * @throws Exception
     */
    public function addSection(array $section, ?string $parentSection = null): AbstractBuild
    {
        foreach ($section as $key => $value) {
            if (isset($parentSection)) {
                $this->sections[$parentSection][$key] = $value;
                continue;
            }
            if (is_array($value)) {
                $this->addSection($value, $key);
                continue;
            }

            // Convert any numeric value to a string
            if (is_numeric($value)) {
                $value = (string) $value;
            }

            $this->sections[$key] = $value;
        }

        return $this;
    }

    /**
     * Output $this->sections as a valid yaml string
     */
    public function export(): string
    {
        $sections = $this->sortSection($this->sections);
        $yamlFile = Yaml::dump($sections, 4, 2);
        return $this->removeNulls($yamlFile);
    }

    /**
     * @throws Exception
     */
    public function getEnvValue(string $envKey): string
    {
        $valueToReturn = getenv($envKey);
        if (empty($valueToReturn)) {
            throw new Exception("$envKey variable not set or empty");
        }
        return $valueToReturn;
    }

    /**
     * Sorts, by key, the docker-compose keys
     *
     *
     * @return string[]
     */
    private function sortSection(array $sectionToBeSorted): array
    {
        foreach ($sectionToBeSorted as $key => $value) {
            if (is_array($value)) {
                $sectionToBeSorted[$key] = $this->sortSection($value);
            }
        }
        // Don't sort the top level, as it looks weird to a human if you do
        if (!empty($sectionToBeSorted['version'])) {
            return $sectionToBeSorted;
        }
        ksort($sectionToBeSorted);
        // Now return our top level, sorted the way we want it
        if (!empty($sectionToBeSorted['services']) && !empty($sectionToBeSorted['networks'])) {
            $orderOfArray = [
                'services' => null,
                'networks' => null,
                'volumes' => null,
            ];
            // Replace the nulls with the real values
            $correctedArray = array_replace($orderOfArray, $sectionToBeSorted);

            // Sometimes volumes is null. Delete it.
            return array_filter($correctedArray);
        }
        return $sectionToBeSorted;
    }

    /**
     * The yaml dumper outputs null, as that is the spec
     * The easiest solution is to remove those string nulls
     */
    private function removeNulls(string $yamlString): string
    {
        return str_replace(' null', '', $yamlString);
    }
}
