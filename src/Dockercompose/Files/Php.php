<?php

declare(strict_types=1);

namespace App\Dockercompose\Files;

use App\DatabaseType;
use App\Dockercompose\Templates\AbstractTemplate;
use App\Dockercompose\Templates\Mysql;
use App\Dockercompose\Templates\Pgsql;
use App\Dockercompose\Templates\Nginx;
use App\Dockercompose\Templates\Php as PhpTemplate;
use App\Dockercompose\Templates\Redis;
use Exception;
use Override;

/**
 * Class Php
 *
 * Builds a PHP docker-compose file with Database connectivity
 *
 * @package App\Files
 */
class Php extends AbstractBuild
{
    /**
     * @inheritDoc
     *
     * @throws Exception
     *
     */
    #[Override]
    public function run(
        string $exportDirectory,
        string $fileName = self::FILENAME,
        ?DatabaseType $databaseType = null,
    ): void {

        // Default to the mysql database type
        $databaseMain = new Mysql();
        $databaseTesting = new Mysql();
        $databaseMainTemplateName = 'mysql';
        $databaseTestingTemplateName = 'mysql-testing';
        $databaseVersion = $this->getEnvValue('MYSQL_VERSION');

        if ($databaseType->getType() === DatabaseType::TYPE_PGSQL) {
            $databaseMain = new Pgsql();
            $databaseTesting = new Pgsql();
            $databaseMainTemplateName = 'pgsql';
            $databaseTestingTemplateName = 'pgsql-testing';
            $databaseVersion = $this->getEnvValue('PGSQL_VERSION');
        }
        // make sure we set the correct docker image tag
        $dockerImageTag = $this->getEnvValue('PHP_BUILD_VERSION') . '-' . $databaseMainTemplateName;

        // Set up the services and networks they're attached to
        $nginx = new Nginx();
        $redis = new Redis();
        $php = new PhpTemplate();

        // Now add the networks
        $nginx->addNetwork(AbstractTemplate::NETWORK_EXTERNAL)
            ->addNetwork(AbstractTemplate::NETWORK_PHP);
        $redis->addNetwork(AbstractTemplate::NETWORK_PHP)
            ->addNetwork(AbstractTemplate::NETWORK_CACHE);
        $php->addNetwork(AbstractTemplate::NETWORK_PHP)
            ->addNetwork(AbstractTemplate::NETWORK_CACHE)
            ->addNetwork(AbstractTemplate::NETWORK_DATABASE);
        $databaseMain->addNetwork(AbstractTemplate::NETWORK_DATABASE);
        $databaseTesting->addNetwork(AbstractTemplate::NETWORK_DATABASE);

        // Now build the compose file
        $this->addSection($nginx->getValue([$this->getEnvValue('NGINX_VERSION')]), 'services');
        $this->addSection($redis->getValue([$this->getEnvValue('REDIS_VERSION')]), 'services');
        $this->addSection($php->getValue([$dockerImageTag]), 'services');

        $this->addSection(
            $databaseMain->setTemplateName($databaseMainTemplateName)
                ->addVolume()
                ->getValue(
                    [$databaseVersion],
                ),
            'services',
        );
        $this->addSection(
            $databaseTesting->setTemplateName($databaseTestingTemplateName)
                ->getValue(
                    [$databaseVersion],
                ),
            'services',
        );

        // Do this last so it's at the bottom
        $this->addSection(
            [
                'networks' => [
                    AbstractTemplate::NETWORK_EXTERNAL => null,
                    AbstractTemplate::NETWORK_PHP => null,
                    AbstractTemplate::NETWORK_CACHE => null,
                    AbstractTemplate::NETWORK_DATABASE => null,
                ],
            ],
        );

        // Volume for the DBs
        $this->addSection(
            [
                'volumes' => [
                    self::DATABASE_VOLUME_NAME => null,
                ],
            ],
        );

        file_put_contents($exportDirectory . $fileName, $this->export());
    }
}
