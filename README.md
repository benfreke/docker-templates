# docker-templates

A collection of Dockerfiles and Docker Compose files for easier local development.

Do not use these in production without optimisation and understanding how to orchestrate your environment effectively.

These are designed to contain the smallest possible packages to allow development.
There are different versions for either a CLI only version, MySQL version, or a Postgres version.

## How to use

1. Go to the [Releases](https://gitlab.com/benfreke/docker-templates/-/releases) page.
1. Select the latest release
1. Select the combination to use of PHP and Database options.
1. Download the tarball to the root directory of the project you want to use it in
1. Type the command `tar -zxvf <downloaded release file>`
1. Go to the URL `http://localhost` to view the running application

## Why should I use this?

Because you want to use a Containerised local environment and share that with a team, but you're not sure where to start.
This repo contains enough to get you started without needing to understand either Docker or Docker Compose.

There are several amazing options out there already, such as Valet for Laravel. 
Use them if they suit your use case better!

## The Docker Images

These are listed in [Docker Hub](https://hub.docker.com/r/benfreke/php).
By default, all images used are the development images. 
All images have a corresponding CI version that does not have XDebug installed and _should_ execute faster.

## Current versions

This is only listing the pinned versions.
Other extensions, such as `ext-json`, come with the operating system and aren't listed here.

- PHP: 8.0.30 | 8.1.31 | 8.2.27 | 8.3.16 | 8.4.3
- Composer: 2.8.4
- XDebug: 3.4.1
- Nginx: 1.26.2
- Redis: 7.4.2
- MySQL: 9.1.0
- Postgres: 17.2
