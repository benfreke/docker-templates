#!/bin/bash

set -euo pipefail

main () {
  # Set the variables needed for Docker builds that aren't using a Docker Compose file
  IMAGE_TAG=benfreke/php:$BUILD_VERSION_PHP84
  export PHP_BUILD_VERSION=$BUILD_VERSION_PHP84
  export DATABASE_TYPE=cli
  # Do this inside here as otherwise we don't have access to the right variable
  . ../scripts/convert_to_short_php.sh

  cd ..
  mkdir -p public

  # Lint
  docker compose -f docker-compose-test.yml run --rm lint-php
  docker compose -f docker-compose-test.yml run --rm lint-phpcs
  docker compose -f docker-compose-test.yml run --rm lint-rector

  # Run phpunit
  docker compose -f docker-compose-test.yml run --rm test-php

  cleanBuildDirectory "$PHP_BUILD_VERSION_SHORT"
  testAllTheThings

  export DATABASE_TYPE=mysql
  testAllTheThings

  export DATABASE_TYPE=pgsql
  testAllTheThings
}

source ./test-php-common.sh
# This copies the .env values into the script but not the shell
eval "$(grep -E -v '^#' env-local-build.env | xargs)" main "$@"

unset PHP_BUILD_VERSION
unset DATABASE_TYPE
