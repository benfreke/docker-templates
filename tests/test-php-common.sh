
# Remove all build files
# We need to keep the .gitignore though
cleanBuildDirectory () {
  echo "Start: clean build directory"
  # Remove all files that are part of the build
  rm -rf build/"$1"/*

  echo "Finished: clean build directory"
}

createBuildDirectory () {
  echo "Start: create build directory"

  mkdir -p build/"$1"/"$2"

  echo "Finished: create build directory"
}

# Test building the docker images for each image
buildDockerimages () {
  echo "Start: build docker images"
  local folderPath="build/$1/$2/"
  local dbtype=""
  if [ "$2" != "cli" ]; then
    dbtype="-"$2
  fi
  echo "$folderPath"
  mkdir -p "$folderPath"
  cp scripts/dockerfiles/php/php"$1"/* "$folderPath"
  cp scripts/dockerfiles/php/common/* "$folderPath"
  cd "$folderPath" || exit
  docker build --check .
  docker build -t "$IMAGE_TAG""$dbtype" .
  docker build -t "$IMAGE_TAG""$dbtype"-ci --target application-base .
  cd ../../..
  echo "Finished: build docker images"
}

# Test all the things
testAllTheThings () {
  createBuildDirectory "$PHP_BUILD_VERSION_SHORT" "$DATABASE_TYPE"
  docker compose -f docker-compose-test.yml run --rm test-build-"$DATABASE_TYPE"
  docker compose -f docker-compose-test.yml run --rm test-lint-dockerfile
  docker compose -f docker-compose-test.yml run --rm test-lint-dockercompose
  buildDockerimages "$PHP_BUILD_VERSION_SHORT" "$DATABASE_TYPE"
  if [ "$DATABASE_TYPE" != "cli" ]; then
    docker compose -f docker-compose-test.yml run --rm test-deploy-nginx
  else
    docker compose -f docker-compose-test.yml run --rm test-deploy-nonginx
  fi
}
