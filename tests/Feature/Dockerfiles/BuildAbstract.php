<?php

declare(strict_types=1);

namespace Tests\Feature\Dockerfiles;

use App\Dockerfile\Files\Php;
use App\Dockerfile\Templates\AbstractTemplate;
use App\Dockerfile\Templates\Mysql;
use App\Dockerfile\Templates\Pgsql17;
use Exception;
use Iterator;
use PHPUnit\Framework\Attributes\DataProvider;
use Spatie\Snapshots\MatchesSnapshots;
use Tests\TestCase;

/**
 * Class EnvTest
 *
 * @package Tests\Feature\Dockerfiles
 */
abstract class BuildAbstract extends TestCase
{
    use MatchesSnapshots;

    protected string $phpBuildVersion;
    protected string $phpShortVersion;
    protected string $phpFilepathVersion;
    protected Php $classUnderTest;

    abstract protected function setupPhpVersionAndClasses(AbstractTemplate|null $databaseClassToUse): void;

    public static function dataTestRun(): Iterator
    {
        yield 'no database' => [
            null,
        ];
        yield 'mysql database' => [
            new Mysql(),
        ];
        yield 'pgsql database' => [
            new Pgsql17(),
        ];
    }

    /**
     * @throws Exception
     */
    #[DataProvider('dataTestRun')]
    public function testRun(AbstractTemplate|null $databaseClassToUse): void
    {
        // Arrange
        $this->setupPhpVersionAndClasses($databaseClassToUse);
        putenv("PHP_BUILD_VERSION=$this->phpBuildVersion");
        putenv("PHP_SHORT_VERSION=$this->phpShortVersion");
        putenv("PHP_FILEPATH_VERSION=$this->phpFilepathVersion");

        $fileName = md5($this->nameWithDataSet()) . '.txt';
        $filePath = sys_get_temp_dir() . '/' . $fileName;

        // Act
        $this->classUnderTest->run(str_replace($fileName, '', $filePath), $fileName);

        // Assert
        $this->assertFileExists($filePath);
        $value = file_get_contents($filePath);

        $this->assertTrue(is_string($value));
        $this->assertStringContainsString($this->phpBuildVersion, $value);
        $this->assertStringContainsString($this->phpShortVersion, $value);
        $this->assertStringContainsString($this->phpFilepathVersion, $value);
        $this->assertMatchesFileSnapshot($filePath);

        // Check that the Dockerfile does or doesn't have tokenizer in it
        if ($this->phpShortVersion === '80') {
            $this->assertStringContainsString('token', $value);
        } else {
            $this->assertStringNotContainsString('token', $value);
        }
    }

    public function testRunException(): void
    {
        $this->setupPhpVersionAndClasses(null);
        putenv('PHP_BUILD_VERSION=');
        $this->expectException(Exception::class);
        $this->classUnderTest->run('baf');
    }
}
