<?php

declare(strict_types=1);

namespace Tests\Feature\Dockerfiles;

use App\Dockerfile\Files\Php;
use App\Dockerfile\Templates\AbstractTemplate;
use App\Dockerfile\Templates\Development;
use App\Dockerfile\Templates\PhpOsInstall;
use Override;

/**
 *
 */
class Php82Test extends BuildAbstract
{
    #[Override]
    protected function setupPhpVersionAndClasses(AbstractTemplate|null $databaseClassToUse): void
    {
        $this->phpBuildVersion = '8.2.0';
        $this->phpShortVersion = '82';
        $this->phpFilepathVersion = '8.2';

        $this->classUnderTest = new Php(new PhpOsInstall(), new Development(), $databaseClassToUse);
    }
}
