# syntax=docker/dockerfile:1
# check=error=true

# This stage is my base PHP image. It does not have composer, etc, as they are not needed for all PHP
FROM php:8.0.2-fpm-alpine AS application-base

# Set the metadata
LABEL maintainer="Ben Freke (https://gitlab.com/benfreke)"

# Add some extensions that we are always going to need
# Install into alpine what we need to support the install
# This is split into build dependencies, and the libraries that actually required
# `docker-php-ext-configure` to use the php docker tools to configure GD
# `--no-cache` to remove cache folders
RUN apk add --no-cache --virtual .build-deps \
        autoconf=~2.71 \
        freetype-dev=~2.12 \
        libjpeg-turbo-dev=~2.1 \
        libpng-dev=~1.6 \
        oniguruma-dev=~6.9 \
        icu-dev=~71.1 \
    # We need PECL for phpredis and xdebug
    && apk add --no-cache --virtual .pecl-deps \
        coreutils=~9.1 \
        g++=~11.2 \
        gcc=~11.2 \
        git=~2.36 \
        make=~4.3 \
        libzip-dev=~1.8 \
    && apk add --no-cache --virtual .required-deps \
        freetype=~2.12 \
        libjpeg-turbo=~2.1 \
        libpng=~1.6 \
        icu-libs=~71.1 \
    && docker-php-ext-configure gd \
        --with-freetype \
        --with-jpeg \
    && NPROC=$(grep -c ^processor /proc/cpuinfo 2>/dev/null || 1) \
    && docker-php-ext-install -j"${NPROC}" \
        gd \
        opcache \
        intl \
        mbstring \
        tokenizer \
        zip

# Install Redis
RUN pecl install -o -f redis \
    && rm -rf /tmp/pear \
    && docker-php-ext-enable \
        redis

# Now install pgsql
RUN apk add --no-cache --virtual .build-pgsql-deps \
            postgresql14-dev=~14.12 \
        && apk add --no-cache --virtual .required-pgsql-deps \
            postgresql14=~14.12 \
    && NPROC=$(grep -c ^processor /proc/cpuinfo 2>/dev/null || 1) \
    && docker-php-ext-install -j"${NPROC}" \
        pdo \
        pdo_pgsql \
        pgsql


# Get a working php-fpm
# This is correct for PHP80 only
COPY php-fpm-80.conf /etc/php/8.0/fpm/php-fpm.conf

# Set the home directory for composer to use after installation
ENV COMPOSER_HOME=/tmp

# https://unix.stackexchange.com/questions/71622/
RUN chmod 1777 /tmp

# Set the version of composer that is installed
ENV COMPOSER_ROOT_VERSION=2.0.6

# Install composer
COPY --from=composer/composer:2.0.6-bin /composer /usr/bin/composer

# Create a group and user so we're not running as root always
RUN addgroup -S appgroup && adduser -S appuser -G appgroup

# Tell docker that all future commands should run as the appuser we just created
USER appuser

# This is my development image. We add xdebug because it is awesome
FROM application-base AS development

# We need to install OS packages as the root user
USER root

# PECL needs this
RUN apk add --no-cache linux-headers=~5.16

# Install xdebug via pecl. In theory we could install from sources, but this is easier
RUN pecl install xdebug-2.9.8 \
    && rm -rf /tmp/pear \
    && docker-php-ext-enable xdebug

# Copy xdebug config and ensure we can use it
COPY xdebug.ini /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

RUN chmod 644 /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    # sed takes a backup, so we need permissions on the folder as well
    && chmod 755 /usr/local/etc/php/conf.d

# The entrypoint will set up xdebug based on the flag and ensure the we have the right number of values in .env files
# It also checks that we have the same keys between .env and .env.example
COPY entrypoint.sh /usr/local/bin/entrypoint.sh
COPY file-compare.php /usr/local/bin/file-compare.php
COPY CompareIniFiles.php /usr/local/bin/CompareIniFiles.php
RUN chmod 664 /usr/local/bin/entrypoint.sh \
    && chmod +x /usr/local/bin/entrypoint.sh \
    && chmod 664 /usr/local/bin/file-compare.php \
    && chmod +x /usr/local/bin/file-compare.php \
    && chmod 664 /usr/local/bin/CompareIniFiles.php

# Set an entrypoint which handles some startup logic
ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]

# Set the working directory for script purposes
WORKDIR /var/www/html

# Set the user to not be root now we've installed everything
USER appuser

