<?php

declare(strict_types=1);

namespace Tests\Feature\Dockerfiles;

use App\Dockerfile\Files\Php;
use App\Dockerfile\Templates\AbstractTemplate;
use App\Dockerfile\Templates\Development80;
use App\Dockerfile\Templates\Pgsql14;
use App\Dockerfile\Templates\Pgsql17;
use App\Dockerfile\Templates\Php80OsInstall;
use Override;

/**
 *
 */
class Php80Test extends BuildAbstract
{
    #[Override]
    protected function setupPhpVersionAndClasses(AbstractTemplate|null $databaseClassToUse): void
    {
        $this->phpBuildVersion = '8.0.2';
        $this->phpShortVersion = '80';
        $this->phpFilepathVersion = '8.0';

        if (!is_null($databaseClassToUse) && $databaseClassToUse instanceof Pgsql17) {
            $databaseClassToUse = new Pgsql14();
        }
        $this->classUnderTest = new Php(new Php80OsInstall(), new Development80(), $databaseClassToUse);
    }
}
