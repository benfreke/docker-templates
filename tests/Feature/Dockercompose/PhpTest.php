<?php

declare(strict_types=1);

namespace Tests\Feature\Dockercompose;

use App\DatabaseType;
use App\Dockercompose\Files\Php;
use Exception;
use Iterator;
use PHPUnit\Framework\Attributes\DataProvider;
use Spatie\Snapshots\MatchesSnapshots;
use Symfony\Component\Yaml\Yaml;
use Tests\TestCase;

/**
 * Test Docker Compose files are correct
 */
class PhpTest extends TestCase
{
    use MatchesSnapshots;

    public static function dataTestRun(): Iterator
    {
        yield 'PHP80-MYSQL' => [
            '8.0.2',
            DatabaseType::TYPE_MYSQL,
        ];
        yield 'PHP80-PGSQL' => [
            '8.0.2',
            DatabaseType::TYPE_PGSQL,
        ];
        yield 'PHP81-MYSQL' => [
            '8.1.2',
            DatabaseType::TYPE_MYSQL,
        ];
        yield 'PHP81-PGSQL' => [
            '8.1.2',
            DatabaseType::TYPE_PGSQL,
        ];
        yield 'PHP82-MYSQL' => [
            '8.2.2',
            DatabaseType::TYPE_MYSQL,
        ];
        yield 'PHP82-PGSQL' => [
            '8.2.2',
            DatabaseType::TYPE_PGSQL,
        ];
        yield 'PHP83-MYSQL' => [
            '8.3.2',
            DatabaseType::TYPE_MYSQL,
        ];
        yield 'PHP83-PGSQL' => [
            '8.3.2',
            DatabaseType::TYPE_PGSQL,
        ];
    }

    /**
     * @throws Exception
     */
    #[DataProvider('dataTestRun')]
    public function testRun(string $phpVersion, int $databaseType): void
    {
        // Arrange
        putenv("PHP_BUILD_VERSION=$phpVersion");
        $php = new Php();
        $fileName = md5($this->nameWithDataSet()) . '.yml';
        $filePath = sys_get_temp_dir() . '/' . $fileName;

        // Act
        $php->run(str_replace($fileName, '', $filePath), $fileName, new DatabaseType($databaseType));

        // Assert
        $value = Yaml::parseFile($filePath);
        $this->assertFileExists($filePath);
        $this->assertTrue(is_array($value));
        $this->assertArrayHasKey('networks', $value);
        $this->assertArrayHasKey('services', $value);
        $this->assertArrayHasKey('volumes', $value);
        $this->assertMatchesFileSnapshot($filePath);
    }
}
