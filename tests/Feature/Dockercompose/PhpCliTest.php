<?php

declare(strict_types=1);

namespace Tests\Feature\Dockercompose;

use App\Dockercompose\Files\PhpCli;
use Exception;
use Iterator;
use PHPUnit\Framework\Attributes\DataProvider;
use Spatie\Snapshots\MatchesSnapshots;
use Symfony\Component\Yaml\Yaml;
use Tests\TestCase;

/**
 *
 */
class PhpCliTest extends TestCase
{
    use MatchesSnapshots;

    public static function dataTestRun(): Iterator
    {
        yield 'PHP80' => [
            '8.0.2',
        ];
        yield 'PHP81' => [
            '8.1.0',
        ];
        yield 'PHP82' => [
            '8.2.0',
        ];
        yield 'PHP83' => [
            '8.3.0',
        ];
    }

    /**
     * @throws Exception
     */
    #[DataProvider('dataTestRun')]
    public function testRun(string $phpVersion): void
    {
        // Arrange
        putenv("PHP_BUILD_VERSION=$phpVersion");
        $phpCli = new PhpCli();
        $fileName = md5($this->nameWithDataSet()) . '.yml';
        $filePath = sys_get_temp_dir() . '/' . $fileName;

        // Act
        $phpCli->run(str_replace($fileName, '', $filePath), $fileName);

        // Assert
        $value = Yaml::parseFile($filePath);
        $this->assertFileExists($filePath);
        $this->assertTrue(is_array($value));
        $this->assertArrayHasKey('networks', $value);
        $this->assertArrayHasKey('services', $value);
        $this->assertMatchesFileSnapshot($filePath);
    }
}
