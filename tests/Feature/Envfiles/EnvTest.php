<?php

declare(strict_types=1);

namespace Tests\Feature\Envfiles;

use App\DatabaseType;
use App\Envfiles\Files\Env;
use Exception;
use Iterator;
use PHPUnit\Framework\Attributes\DataProvider;
use Spatie\Snapshots\MatchesSnapshots;
use Tests\TestCase;

/**
 *
 */
class EnvTest extends TestCase
{
    use MatchesSnapshots;

    protected string $dbConnection;
    protected string $dbHost;
    protected string $dbPort;
    protected string $dbDatabase;
    protected string $dbUsername;
    protected string $dbPassword;

    public static function dataTestRun(): Iterator
    {
        yield 'No database set' => [
            DatabaseType::TYPE_NONE,
        ];
        yield 'MYSQL Database' => [
            DatabaseType::TYPE_MYSQL,
        ];
        yield 'PGSQL Database' => [
            DatabaseType::TYPE_PGSQL,
        ];
        yield 'No database set, testing true' => [
            DatabaseType::TYPE_NONE,
            true,
        ];
        yield 'MYSQL Database, testing true' => [
            DatabaseType::TYPE_MYSQL,
            true,
        ];
        yield 'PGSQL Database, testing true' => [
            DatabaseType::TYPE_PGSQL,
            true,
        ];
    }

    /**
     * @throws Exception
     */
    #[DataProvider('dataTestRun')]
    public function testRun(int $dbType, bool $testing = false): void
    {
        // Arrange
        $fileName = md5($this->nameWithDataSet()) . '.env';
        $filePath = sys_get_temp_dir() . DIRECTORY_SEPARATOR . $fileName;
        $env = new Env(sys_get_temp_dir(), new DatabaseType($dbType), $fileName, $testing);

        // Act
        $env->run();

        // Assert
        $this->assertMatchesFileSnapshot($filePath);
    }
}
