#!/bin/sh

set -euo pipefail

# This copies the .env values into the script but not the shell
eval "$(grep -E -v '^#' env-local-build.env | xargs)" docker compose -f ../docker-compose-test.yml config
