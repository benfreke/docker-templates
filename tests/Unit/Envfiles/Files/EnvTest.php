<?php

declare(strict_types=1);

namespace Tests\Unit\Envfiles\Files;

use App\DatabaseType;
use App\Envfiles\Files\Env;
use Iterator;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

/**
 *
 */
class EnvTest extends TestCase
{
    public static function dataTestRun(): Iterator
    {
        yield 'MYSQL Database' => [
            new DatabaseType(DatabaseType::TYPE_MYSQL),
            [
                'DB_CONNECTION=mysql',
                'COMPARE_ENV_FILES=1',
            ],
            [
                'pgsql',
                'DB_HOST=127.0.0.1',
                '-testing',
            ],
        ];
        yield 'No database' => [
            new DatabaseType(DatabaseType::TYPE_NONE),
            [
                'DB_HOST=127.0.0.1',
                'COMPARE_ENV_FILES=1',
            ],
            [
                'pgsql',
                '-testing',
            ],
        ];
        yield 'PGSQL' => [
            new DatabaseType(DatabaseType::TYPE_PGSQL),
            [
                'DB_HOST=pgsql',
                'COMPARE_ENV_FILES=1',
            ],
            [
                'mysql',
                '-testing',
            ],
        ];
    }

    #[DataProvider('dataTestRun')]
    public function testRun(DatabaseType $databaseType, array $assertExists, array $assertDoesntExist): void
    {
        // arrange
        $fileName = md5($this->nameWithDataSet()) . '.txt';
        $filePath = sys_get_temp_dir() . '/' . $fileName;
        $env = new Env(sys_get_temp_dir(), $databaseType, $fileName);

        // act
        $env->run();

        // assert
        $output = file_get_contents($filePath);
        $this->assertEquals(true, is_string($output));

        foreach ($assertExists as $valueToAssert) {
            $this->assertStringContainsString($valueToAssert, $output);
        }

        foreach ($assertDoesntExist as $valueToAssert) {
            $this->assertStringNotContainsString($valueToAssert, $output);
        }
    }

    public function testRunWithTest(): void
    {
        // arrange
        $fileName = md5($this->nameWithDataSet()) . '.txt';
        $filePath = sys_get_temp_dir() . '/' . $fileName;
        $env = new Env(sys_get_temp_dir(), new DatabaseType(DatabaseType::TYPE_PGSQL), $fileName, true);

        // act
        $env->run();

        // assert
        $output = file_get_contents($filePath);
        $this->assertEquals(true, is_string($output));
        $this->assertStringContainsString("DB_HOST=pgsql-testing\n", $output);
    }
}
