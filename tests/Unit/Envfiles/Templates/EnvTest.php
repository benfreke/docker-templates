<?php

declare(strict_types=1);

namespace Tests\Unit\Envfiles\Templates;

use App\Envfiles\Templates\Env;
use Iterator;
use PHPUnit\Framework\Attributes\DataProvider;
use Tests\TestCase;

/**
 * Tests Env template
 */
class EnvTest extends TestCase
{
    public static function dataGetValue(): Iterator
    {
        yield 'no replacements' => [
            'template string',
            'template string',
            ['foo'],
            ['bar'],
        ];
        yield 'replace a single value' => [
            'foo string',
            'bar string',
            ['foo'],
            ['bar'],
        ];
        yield 'Empty search - no replacement' => [
            'foo',
            'foo',
            [],
            ['bar'],
        ];
        yield 'Empty search - replacement' => [
            '%%%%',
            'bar',
            [],
            ['bar'],
        ];
        yield 'Multiple replacements' => [
            'first second',
            'third fourth',
            ['first', 'second'],
            ['third', 'fourth'],
        ];
    }


    #[DataProvider('dataGetValue')]
    public function testGetValue(string $template, string $expectedResult, array $search, array $replace): void
    {
        // arrange
        $env = new Env();

        // act
        $env->setTemplate($template);
        $result = $env->getValue($replace, $search);

        // assert
        $this->assertSame($expectedResult, $result);
    }
}
