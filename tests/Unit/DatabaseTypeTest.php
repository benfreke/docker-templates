<?php

declare(strict_types=1);

namespace Tests\Unit;

use App\DatabaseType;
use Iterator;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

/**
 * Ensure my DatabaseType class is correct
 */
class DatabaseTypeTest extends TestCase
{
    public static function dataDatabaseType(): Iterator
    {
        yield 'No database' => [
            1,
            DatabaseType::TYPE_NONE,
        ];
        yield 'Mysql' => [
            2,
            DatabaseType::TYPE_MYSQL,
        ];
        yield 'Pgsql' => [
            3,
            DatabaseType::TYPE_PGSQL,
        ];
        yield 'Invalid integer' => [
            0,
            DatabaseType::TYPE_NONE,
        ];
    }


    #[DataProvider('dataDatabaseType')]
    public function testDatabaseType(int $type, int $typeResult): void
    {
        $databaseType = new DatabaseType($type);
        $this->assertSame($typeResult, $databaseType->getType());
    }
}
