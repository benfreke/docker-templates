<?php

declare(strict_types=1);

namespace Tests\Unit\Dockerfile\Templates;

use App\Dockerfile\Templates\AbstractTemplate;
use App\Dockerfile\Templates\Php;
use Override;

/**
 *
 */
class PhpTest extends TemplateAbstract
{
    #[Override]
    protected function getClassUnderTest(): AbstractTemplate
    {
        return new Php();
    }
}
