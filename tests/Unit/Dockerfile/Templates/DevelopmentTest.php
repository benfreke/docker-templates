<?php

declare(strict_types=1);

namespace Tests\Unit\Dockerfile\Templates;

use App\Dockerfile\Templates\AbstractTemplate;
use App\Dockerfile\Templates\Development80;
use Override;

/**
 *
 */
class DevelopmentTest extends TemplateAbstract
{
    #[Override]
    protected function getClassUnderTest(): AbstractTemplate
    {
        return new Development80();
    }
}
