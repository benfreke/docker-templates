<?php

declare(strict_types=1);

namespace Tests\Unit\Dockerfile\Templates;

use App\Dockerfile\Templates\AbstractTemplate;
use App\Dockerfile\Templates\Pgsql14;
use Override;

/**
 *
 */
class PgsqlTest extends TemplateAbstract
{
    #[Override]
    protected function getClassUnderTest(): AbstractTemplate
    {
        return new Pgsql14();
    }
}
