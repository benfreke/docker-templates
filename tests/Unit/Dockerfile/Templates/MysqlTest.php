<?php

declare(strict_types=1);

namespace Tests\Unit\Dockerfile\Templates;

use App\Dockerfile\Templates\AbstractTemplate;
use App\Dockerfile\Templates\Mysql;
use Override;

/**
 * Tests the Mysql template
 */
class MysqlTest extends TemplateAbstract
{
    #[Override]
    protected function getClassUnderTest(): AbstractTemplate
    {
        return new Mysql();
    }
}
