<?php

declare(strict_types=1);

namespace Tests\Unit\Dockerfile\Templates;

use App\Dockerfile\Templates\AbstractTemplate;
use App\TemplateInterface;
use Iterator;
use PHPUnit\Framework\Attributes\DataProvider;
use Tests\TestCase;

/**
 * Tests that the Dockerfile logic works
 */
abstract class TemplateAbstract extends TestCase
{
    abstract protected function getClassUnderTest(): AbstractTemplate;

    public function testBaseAbstractInheritance(): void
    {
        $this->assertInstanceOf(AbstractTemplate::class, $this->getClassUnderTest());
        $this->assertInstanceOf(TemplateInterface::class, $this->getClassUnderTest());
    }

    public static function dataGetValue(): Iterator
    {
        yield 'empty test' => [
            '',
            '',
        ];
        yield 'No replacing' => [
            'asdf',
            'asdf',
        ];
        yield 'Replace nothing as search not found' => [
            'This is a template',
            'This is a template',
            [null],
        ];
        yield 'Replace the default' => [
            'This is a %%%%',
            'This is a template',
            ['template'],
        ];
        yield 'Replace a non default' => [
            'This is a asdf',
            'This is a template',
            ['template'],
            ['asdf'],
        ];
        yield 'Replace two values' => [
            'This is one and two',
            'This is three and four',
            ['three', 'four'],
            ['one', 'two'],
        ];
    }

    /**
     * Ensure the replacements work as expected
     *
     */
    #[DataProvider('dataGetValue')]
    public function testGetValue(
        string $inputTemplate,
        string $result,
        array $replacement = [null],
        array $search = [],
    ): void {
        $template = $this->getClassUnderTest();
        $template->setTemplate($inputTemplate);
        $computedResult = $template->getValue($replacement, $search);
        $this->assertSame($result, $computedResult);
    }
}
