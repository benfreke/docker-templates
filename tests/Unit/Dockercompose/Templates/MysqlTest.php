<?php

declare(strict_types=1);

namespace Tests\Unit\Dockercompose\Templates;

use App\Dockercompose\Templates\Mysql;
use Override;

/**
 *
 */
class MysqlTest extends TemplateAbstract
{
    #[Override]
    protected function getClassUnderTest(): Mysql
    {
        return new Mysql();
    }

    public function testAddVolume(): void
    {
        // Create the template
        $mysql = $this->getClassUnderTest();
        $templateName = 'abc';
        $mysql->setTemplateName($templateName);
        $keyToSearchFor = 'volumes';

        $templateValue = $mysql->getValue();

        $mysql->setTemplate([]);

        $this->assertEmpty($mysql->getValue());

        // Now put the template back
        $mysql->setTemplate($templateValue);

        // Get it first and assert it has no volume
        $this->assertArrayNotHasKey(
            $keyToSearchFor,
            // @phpstan-ignore-next-line
            $mysql->getValue()[$templateName],
        );

        $mysql->addVolume('');

        // Assert we have no volume after adding an empty value
        $this->assertArrayNotHasKey(
            $keyToSearchFor,
            // @phpstan-ignore-next-line
            $mysql->getValue()[$templateName],
        );

        // Add the volume
        $mysql->addVolume('my-volume');

        // Assert it now exists!
        $this->assertArrayHasKey(
            $keyToSearchFor,
            // @phpstan-ignore-next-line
            $mysql->getValue()[$templateName],
        );
    }
}
