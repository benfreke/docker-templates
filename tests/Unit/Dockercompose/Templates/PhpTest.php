<?php

declare(strict_types=1);

namespace Tests\Unit\Dockercompose\Templates;

use App\Dockercompose\Templates\Php;
use Override;

/**
 *
 */
class PhpTest extends TemplateAbstract
{
    #[Override]
    protected function getClassUnderTest(): Php
    {
        return new Php();
    }
}
