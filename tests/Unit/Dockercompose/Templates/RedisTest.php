<?php

declare(strict_types=1);

namespace Tests\Unit\Dockercompose\Templates;

use App\Dockercompose\Templates\Redis;
use Override;

/**
 *
 */
class RedisTest extends TemplateAbstract
{
    #[Override]
    protected function getClassUnderTest(): Redis
    {
        return new Redis();
    }
}
