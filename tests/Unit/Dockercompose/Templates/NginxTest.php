<?php

declare(strict_types=1);

namespace Tests\Unit\Dockercompose\Templates;

use App\Dockercompose\Templates\Nginx;
use Override;

/**
 *
 */
class NginxTest extends TemplateAbstract
{
    #[Override]
    protected function getClassUnderTest(): Nginx
    {
        return new Nginx();
    }
}
