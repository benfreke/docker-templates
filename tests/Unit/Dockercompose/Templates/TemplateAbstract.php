<?php

declare(strict_types=1);

namespace Tests\Unit\Dockercompose\Templates;

use App\Dockercompose\Templates\AbstractTemplate;
use App\TemplateInterface;
use Exception;
use InvalidArgumentException;
use Iterator;
use PHPUnit\Framework\Attributes\DataProvider;
use Tests\TestCase;

/**
 * Class TemplateTest
 *
 * @package Tests\Unit\Dockercompose\Templates
 */
abstract class TemplateAbstract extends TestCase
{
    abstract protected function getClassUnderTest(): AbstractTemplate;

    public function testBaseAbstractInheritance(): void
    {
        $this->assertInstanceOf(AbstractTemplate::class, $this->getClassUnderTest());
        $this->assertInstanceOf(TemplateInterface::class, $this->getClassUnderTest());
    }

    public static function dataGetValue(): Iterator
    {
        yield 'empty test' => [
            [],
            [],
        ];
        yield 'No replacing' => [
            ['asdf'],
            ['asdf'],
        ];
        yield 'Replace nothing as search not found' => [
            ['This is a template'],
            ['This is a template'],
            [null],
        ];
        yield 'Replace the default' => [
            ['This is a %%%%'],
            ['This is a template'],
            ['template'],
        ];
        yield 'Replace a non default' => [
            ['This is a asdf'],
            ['This is a template'],
            ['template'],
            ['asdf'],
        ];
        yield 'Replace two values' => [
            ['This is one and two'],
            ['This is three and four'],
            ['three', 'four'],
            ['one', 'two'],
        ];
        yield 'Replace a value inside the array' => [
            [
                'deep' => [
                    'one' => '%%%%',
                ],
            ],
            [
                'deep' => [
                    'one' => 'aaaa',
                ],
            ],
            ['aaaa'],
        ];
        yield 'Ensure booleans are safe' => [
            ['key' => true],
            ['key' => true],
            ['not replaced'],
        ];
        yield 'Ensure booleans are safe in an array' => [
            ['key' => ['asfd' => true]],
            ['key' => ['asfd' => true]],
            ['not replaced'],
        ];
    }

    /**
     * Ensure the replacements work as expected
     *
     */
    #[DataProvider('dataGetValue')]
    public function testGetValue(
        array $inputTemplate,
        array $result,
        array $replacement = [null],
        ?array $search = null,
    ): void {
        $template = $this->getClassUnderTest();
        $template->setTemplate($inputTemplate);
        $computedResult = $template->getValue($replacement, $search);
        $this->assertSame($result, $computedResult);
        $this->assertIsArray($result);
    }

    public static function dataAddNetwork(): Iterator
    {
        yield 'Add a single network' => ['FOOBAR'];
        yield 'Add the same network twice' => [['FOOBAR', 'FOOBAR']];
        yield 'Add two different networks' => [['FOOBAR', 'BARFOO'], 2];
        yield 'Adds three but only 2 unique' => [['FOOBAR', 'BARFOO', 'FOOBAR'], 2];
    }

    /**
     *
     *
     *
     * @throws Exception
     */
    #[DataProvider('dataAddNetwork')]
    public function testAddNetwork(string|array $networksToAdd, int $totalNetworks = 1): void
    {
        $networksArrayKey = 'networks';
        if (is_string($networksToAdd)) {
            $networksToAdd = [$networksToAdd];
        }
        $template = $this->getClassUnderTest();
        $originalTemplate = $template->getValue();
        $serviceName = array_key_first($originalTemplate);

        // Make sure nothing exists right now
        $this->assertArrayNotHasKey($networksArrayKey, $template->getValue([])[$serviceName]);

        foreach ($networksToAdd as $networkToAdd) {
            $template->addNetwork($networkToAdd);
        }

        $this->assertArrayHasKey($networksArrayKey, $template->getValue()[$serviceName]);
        $this->assertCount($totalNetworks, $template->getValue()[$serviceName][$networksArrayKey]);
    }

    public function testAddNetworkNameException(): void
    {
        $this->expectException(Exception::class);
        $template = $this->getClassUnderTest();
        $template->setTemplate([]);
        $template->addNetwork('random');
    }

    public function testSetTemplateName(): void
    {
        $template = $this->getClassUnderTest();
        $originalTemplate = $template->getValue();
        $serviceName = array_key_first($originalTemplate);

        $template->setTemplateName('foo');
        $this->assertNotTrue(isset($originalTemplate['foo']));
        $this->assertArrayHasKey($serviceName, $originalTemplate);

        $template->setTemplateName('foo', $serviceName);
        $originalTemplate = $template->getValue();
        $this->assertArrayHasKey('foo', $originalTemplate);
        $this->assertNotTrue(isset($originalTemplate[$serviceName]));
    }

    public function testSetTemplateNameException(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $template = $this->getClassUnderTest();
        $template->setTemplateName('');
    }
}
