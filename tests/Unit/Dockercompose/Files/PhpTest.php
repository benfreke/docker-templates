<?php

declare(strict_types=1);

namespace Tests\Unit\Dockercompose\Files;

use App\Dockercompose\Files\AbstractBuild;
use App\Dockercompose\Files\Php;
use Override;

/**
 *
 */
class PhpTest extends PhpAbstract
{
    #[Override]
    protected function getClassUnderTest(): AbstractBuild
    {
        return new Php();
    }
}
