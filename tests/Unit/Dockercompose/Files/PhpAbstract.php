<?php

declare(strict_types=1);

namespace Tests\Unit\Dockercompose\Files;

use App\BuildFileInterface;
use App\Dockercompose\Files\AbstractBuild;
use Exception;
use Iterator;
use PHPUnit\Framework\Attributes\DataProvider;
use Tests\TestCase;

/**
 * Class BuildTest
 *
 * @package Tests\Unit
 */
abstract class PhpAbstract extends TestCase
{
    public function testBaseAbstractInheritance(): void
    {
        $this->assertInstanceOf(AbstractBuild::class, $this->getClassUnderTest());
        $this->assertInstanceOf(BuildFileInterface::class, $this->getClassUnderTest());
    }

    public static function dataAddSection(): Iterator
    {
        yield 'Top level only' => [
            ['version' => '3.7'],
            "version: '3.7'\n",
        ];
        yield 'Top level only with a number' => [
            ['version' => 3.7],
            "version: '3.7'\n",
        ];
        yield 'Multiple level' => [
            [
                'top' => [
                    'version' => '3.7',
                ],
            ],
            "top:\n  version: '3.7'\n",
        ];
        yield 'Just the key' => [
            ['version' => null],
            "version:\n",
        ];
        yield 'Multiple top level tests' => [
            [
                'version' => '3.7',
                'network' => [
                    'php' => null,
                ],
            ],
            "version: '3.7'\nnetwork:\n  php:\n",
        ];
    }

    public static function dataTestSort(): Iterator
    {
        yield 'No sorting required' => [
            [
                'network' => [
                    'php' => null,
                ],
                'version' => '3.7',
            ],
            "network:\n  php:\nversion: '3.7'\n",
        ];
        yield 'Top level sorted, children not' => [
            [
                'version' => '3.7',
                'network' => [
                    'redis' => null,
                    'php' => null,
                ],
            ],
            "version: '3.7'\nnetwork:\n  php:\n  redis:\n",
        ];
        yield 'Top level sorted, children also' => [
            [
                'version' => '3.7',
                'network' => [
                    'php' => null,
                    'redis' => null,
                ],
            ],
            "version: '3.7'\nnetwork:\n  php:\n  redis:\n",
        ];
    }

    /**
     *
     *
     * @throws Exception
     */
    #[DataProvider('dataTestSort')]
    #[DataProvider('dataAddSection')]
    public function testAddSection(array $section, string $result): void
    {
        $buildTest = $this->getClassUnderTest();

        $buildTest->addSection($section);

        $this->assertSame($result, $buildTest->export());
    }

    public static function dataAddSectionMultipleChildren(): Iterator
    {
        yield 'Multiple children' => [
            [
                'php' => null,
                'public' => null,
            ],
            'parent',
            "parent:\n  php:\n  public:\n",
        ];
        yield 'Multiple children with another level' => [
            [
                'php' => [
                    'random' => 'string string',
                ],
                'public' => null,
            ],
            'parent',
            "parent:\n  php:\n    random: 'string string'\n  public:\n",
        ];
    }

    /**
     *
     * @throws Exception
     */
    #[DataProvider('dataAddSectionMultipleChildren')]
    public function testAddSectionWithParent(array $section, string $parent, string $result): void
    {
        $buildTest = $this->getClassUnderTest();

        $buildTest->addSection($section, $parent);

        $this->assertSame($result, $buildTest->export());
    }


    abstract protected function getClassUnderTest(): AbstractBuild;

    public static function dataGetEnvValueException(): Iterator
    {
        yield 'Empty string' => [''];
        yield 'Non-existing value' => ['BAF_ENV_VALUE'];
    }

    #[DataProvider('dataGetEnvValueException')]
    public function testGetEnvValueException(string $valueToTest): void
    {
        // Arrange
        $buildTest = $this->getClassUnderTest();
        $this->expectException(Exception::class);
        $buildTest->getEnvValue($valueToTest);
    }

    public static function dataGetEnvValue(): Iterator
    {
        yield 'String env value' => [
            'BAF',
            'BAF',
        ];
        yield 'Integer env value' => [
            1,
            '1',
        ];
        yield 'Boolean env value' => [
            true,
            '1',
        ];
    }

    /**
     * @throws Exception
     */
    #[DataProvider('dataGetEnvValue')]
    public function testGetEnvValue(string|int|bool $valueToSet, string $valueToTest): void
    {
        $buildTest = $this->getClassUnderTest();
        putenv("BAF_ENV_KEY=$valueToSet");
        $this->assertSame($valueToTest, $buildTest->getEnvValue('BAF_ENV_KEY'));
    }
}
