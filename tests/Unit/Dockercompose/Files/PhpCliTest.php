<?php

declare(strict_types=1);

namespace Tests\Unit\Dockercompose\Files;

use App\Dockercompose\Files\AbstractBuild;
use App\Dockercompose\Files\PhpCli;
use Override;

/**
 *
 */
class PhpCliTest extends PhpAbstract
{
    #[Override]
    protected function getClassUnderTest(): AbstractBuild
    {
        return new PhpCli();
    }
}
