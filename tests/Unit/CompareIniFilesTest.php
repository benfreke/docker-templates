<?php

declare(strict_types=1);

namespace Tests\Unit;

use Tests\TestCase;
use CompareIniFiles;

/**
 *
 */
class CompareIniFilesTest extends TestCase
{
    public function testExecuteSameKeys(): void
    {
        // Arrange
        $compareIniFiles = new CompareIniFiles(
            __DIR__
            . DIRECTORY_SEPARATOR
            . 'Fixtures'
            . DIRECTORY_SEPARATOR
            . 'ExecuteSameKeys',
        );

        // Act
        $result = $compareIniFiles->execute();

        // Assert
        $this->assertIsArray($result);
        $this->assertEmpty($result);
    }

    public function testExecuteComments(): void
    {
        // Arrange
        $compareIniFiles = new CompareIniFiles(
            __DIR__
            . DIRECTORY_SEPARATOR
            . 'Fixtures'
            . DIRECTORY_SEPARATOR
            . 'ExecuteComments',
        );

        // Act
        $result = $compareIniFiles->execute();

        // Assert
        $this->assertIsArray($result);
        $this->assertEmpty($result);
    }

    public function testExecuteDifferentKeys(): void
    {
        // Arrange
        $compareIniFiles = new CompareIniFiles(
            __DIR__
            . DIRECTORY_SEPARATOR
            . 'Fixtures'
            . DIRECTORY_SEPARATOR
            . 'ExecuteDifferentKeys',
        );

        // Act
        $result = $compareIniFiles->execute();

        // Assert
        $this->assertIsArray($result);
        $this->assertCount(6, $result);

        $this->assertStringEndsWith('bar', $result[0]);
        $this->assertStringStartsWith('.env.test1 has', $result[0]);
        $this->assertStringContainsString('.env.test2 does not', $result[0]);

        $this->assertStringEndsWith('bar', $result[1]);
        $this->assertStringStartsWith('.env.test1 has', $result[1]);
        $this->assertStringContainsString('.env.test3 does not', $result[1]);

        $this->assertStringEndsWith('fez, baz', $result[2]);
        $this->assertStringStartsWith('.env.test2 has', $result[2]);
        $this->assertStringContainsString('.env.test3 does not', $result[2]);

        $this->assertStringEndsWith('fez, baz', $result[3]);
        $this->assertStringStartsWith('.env.test2 has', $result[3]);
        $this->assertStringContainsString('.env.test1 does not', $result[3]);

        $this->assertStringEndsWith('foobar', $result[4]);
        $this->assertStringStartsWith('.env.test3 has', $result[4]);
        $this->assertStringContainsString('.env.test2 does not', $result[4]);

        $this->assertStringEndsWith('foobar', $result[5]);
        $this->assertStringStartsWith('.env.test3 has', $result[5]);
        $this->assertStringContainsString('.env.test1 does not', $result[5]);
    }
}
