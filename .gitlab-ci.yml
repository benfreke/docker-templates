workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
      when: never
    - if: $CI_COMMIT_BRANCH

stages:
  - lint
  - test
  - build
  - lint-output
  - build-dockerfiles
  - deploy
  - release

include:
  - template: Code-Quality.gitlab-ci.yml

variables:
  COMPOSER_HOME: "$CI_PROJECT_DIR/.cache/composer"
  BUILD_VERSION_PHP80: 8.0.30
  BUILD_VERSION_PHP81: 8.1.31
  BUILD_VERSION_PHP82: 8.2.27
  BUILD_VERSION_PHP83: 8.3.16
  BUILD_VERSION_PHP84: 8.4.3
  COMPOSER_VERSION: 2.8.4
  XDEBUG_VERSION: 3.4.1
  NGINX_VERSION: 1.26.2
  REDIS_VERSION: 7.4.2
  MYSQL_VERSION: 9.1.0
  PGSQL_VERSION: "17.2"

.composer-cache: &composer-cache
  cache:
    key:
      files:
        - composer.lock
    paths:
      - vendor/
  variables:
    COMPOSER_HOME: $COMPOSER_HOME

.php-ci-image: &php-ci-image
  image:
#    name: registry.gitlab.com/benfreke/docker-templates/php:8.3.8-ci
    name: benfreke/php:8.4.2-ci
    entrypoint: [ "" ]

.php-image: &php-image
  image:
#    name: registry.gitlab.com/benfreke/docker-templates/php:8.3.8
    name: benfreke/php:8.4.2
    entrypoint: [ "" ]

.parallel-matrix: &parallel-matrix
  parallel:
    matrix:
      - PHP_BUILD_VERSION: [ '${BUILD_VERSION_PHP80}', '${BUILD_VERSION_PHP81}', '${BUILD_VERSION_PHP82}', '${BUILD_VERSION_PHP83}', '${BUILD_VERSION_PHP84}']
        DATABASE_TYPE: [ 'cli', 'mysql', 'pgsql' ]

# For web based services
.parallel-matrix-deploy-nginx: &parallel-matrix-deploy-nginx
  parallel:
    matrix:
      - PHP_BUILD_VERSION: [ '${BUILD_VERSION_PHP80}', '${BUILD_VERSION_PHP81}', '${BUILD_VERSION_PHP82}', '${BUILD_VERSION_PHP83}', '${BUILD_VERSION_PHP84}' ]
        DATABASE_TYPE: [ 'mysql', 'pgsql' ]

# for non web based services
.parallel-matrix-deploy-nonginx: &parallel-matrix-deploy-nonginx
  parallel:
    matrix:
      - PHP_BUILD_VERSION: [ '${BUILD_VERSION_PHP80}', '${BUILD_VERSION_PHP81}', '${BUILD_VERSION_PHP82}', '${BUILD_VERSION_PHP83}', '${BUILD_VERSION_PHP84}' ]
        DATABASE_TYPE: [ 'mysql', 'pgsql' ]

validphp:
  <<: *php-ci-image
  <<: *composer-cache
  stage: lint
  script:
    - composer run-script lint-php

phpcs:
  <<: *php-ci-image
  <<: *composer-cache
  stage: lint
  variables:
    PHP_CS_FIXER_IGNORE_ENV: 1
  script:
    - composer run-script lint-phpcs

phpstan:
  <<: *php-ci-image
  <<: *composer-cache
  stage: lint
  artifacts:
    reports:
      junit: reports/junit-phpstan.xml
  script:
    - composer run-script lint-phpstan

rector:
  <<: *php-ci-image
  <<: *composer-cache
  stage: lint
  script:
    - composer run-script lint-rector
  artifacts:
    reports:
      codequality: reports/rector-report.json
    when: always

test:php:
  <<: *php-image
  <<: *composer-cache
  stage: test
  script:
    - composer run-script test-coverage
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: reports/cobertura.xml
      junit: reports/junit-phpunit.xml
  coverage: '/^\s*Lines:\s*\d+.\d+\%/'
  needs:
    - phpcs
    - validphp
    - phpstan
    - rector

build:
  <<: *php-ci-image
  <<: *composer-cache
  <<: *parallel-matrix
  stage: build
  # Save all the Dockerfile needed files
  artifacts:
    paths:
      - build/
    expire_in: 1 days
    when: on_success
  needs:
    - test:php
  script:
    - source scripts/convert_to_short_php.sh
    - mkdir -p build/$PHP_BUILD_VERSION_SHORT/$DATABASE_TYPE
    - composer install
    - php build-php-$DATABASE_TYPE.php

lint:dockerfile-hadolint:
  <<: *parallel-matrix
  stage: lint-output
  image: hadolint/hadolint:latest-alpine
  script:
    - source scripts/convert_to_short_php.sh
    - cd build/$PHP_BUILD_VERSION_SHORT/$DATABASE_TYPE
    - hadolint Dockerfile
  # Until variables get expanded, we need to get all artifacts
  # todo get the single job rather than all
  needs:
    - build

lint:dockerfile:
  <<: *parallel-matrix
  stage: lint-output
  image: docker:cli
  services:
    - docker:dind
  script:
    - source scripts/convert_to_short_php.sh
    - cd build/$PHP_BUILD_VERSION_SHORT/$DATABASE_TYPE
    - docker build --check .
  # Until variables get expanded, we need to get all artifacts
  # todo get the single job rather than all
  needs:
    - build

lint:dockercompose:
  <<: *parallel-matrix
  stage: lint-output
  image: docker:cli
  script:
    - source scripts/convert_to_short_php.sh
    - cd build/$PHP_BUILD_VERSION_SHORT/$DATABASE_TYPE
    - docker-compose config
  # Until variables get expanded, we need to get all artifacts
  # todo get the single job rather than all
  needs:
    - build

# This is for the push of the images to image repositories
.only-run-on-release: &only-run-on-release
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: never
    - if: $CI_COMMIT_BRANCH == "main"

# This is for only the build of the images, not the push to repositories
.dont-run-on-release: &dont-run-on-release
  rules:
    - if: $CI_COMMIT_BRANCH == "main"
      when: never
    - when: on_success

.dockerfile-script-copy-files: &dockerfile-script-copy-files
  - source scripts/convert_to_short_php.sh
  - folderPath="build/$PHP_BUILD_VERSION_SHORT/$DATABASE_TYPE"
  - dbType="" && [[ $DATABASE_TYPE != "cli" ]] && dbType="-"$DATABASE_TYPE
  - mv scripts/dockerfiles/php/php$PHP_BUILD_VERSION_SHORT/* $folderPath
  - mv scripts/dockerfiles/php/common/* $folderPath
  - cd $folderPath
  - docker build -t "$IMAGE_TAG""$dbType" .
  - docker build -t "$IMAGE_TAG""$dbType"-ci --target application-base .

build:dockerfile:
  <<: *dont-run-on-release
  <<: *parallel-matrix
  stage: build-dockerfiles
  image: docker:stable
  # Until variables get expanded, we need to get all artifacts
  # todo get the single job rather than all
  needs:
    - lint:dockerfile
    - lint:dockercompose
    - build
  services:
    - docker:dind
  variables:
    IMAGE_TAG: benfreke/php:$PHP_BUILD_VERSION
  script:
    - *dockerfile-script-copy-files

deploy:dockerfile:
  <<: *only-run-on-release
  <<: *parallel-matrix
  stage: deploy
  image: docker:stable
  needs:
    - lint:dockerfile
    - lint:dockercompose
    - build
  services:
    - docker:dind
  variables:
    IMAGE_TAG: benfreke/php:$PHP_BUILD_VERSION
    IMAGE_TAG_GITLAB: $CI_REGISTRY_IMAGE/php:$PHP_BUILD_VERSION
  script:
    - *dockerfile-script-copy-files
    - echo -n "$DOCKER_HUB_TOKEN" | docker login -u benfreke --password-stdin docker.io
    - docker push "$IMAGE_TAG""$dbType"
    - docker push "$IMAGE_TAG""$dbType"-ci
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker image tag "$IMAGE_TAG""$dbType" "$IMAGE_TAG_GITLAB""$dbType"
    - docker image tag "$IMAGE_TAG""$dbType"-ci "$IMAGE_TAG_GITLAB""$dbType"-ci
    - docker push "$IMAGE_TAG_GITLAB""$dbType"
    - docker push "$IMAGE_TAG_GITLAB""$dbType"-ci

.deploy:artifact:common: &deploy-artifact-common
  <<: *php-ci-image
  <<: *only-run-on-release
  stage: deploy
  needs:
    - lint:dockerfile
    - lint:dockercompose
    - build
  artifacts:
    expire_in: 7 days
    paths:
      - public/
    when: on_success

deploy:artifact:nginx:
  <<: *deploy-artifact-common
  <<: *parallel-matrix-deploy-nginx
  script:
    - source scripts/convert_to_short_php.sh
    - mkdir -p build/$PHP_BUILD_VERSION_SHORT/$DATABASE_TYPE/scripts/nginx/conf.d/
    - mkdir public
    - cp scripts/nginx/conf.d/laravel.conf build/$PHP_BUILD_VERSION_SHORT/$DATABASE_TYPE/scripts/nginx/conf.d/laravel.conf
    - cd build/$PHP_BUILD_VERSION_SHORT/$DATABASE_TYPE
    - tar -zcvf ../../../public/php$PHP_BUILD_VERSION_SHORT-$DATABASE_TYPE.tgz docker-compose.yml scripts/* .env .env.example .env.testing

deploy:artifact:nonginx:
  <<: *deploy-artifact-common
  <<: *parallel-matrix-deploy-nonginx
  script:
    - source scripts/convert_to_short_php.sh
    - mkdir -p build/$PHP_BUILD_VERSION_SHORT/$DATABASE_TYPE
    - mkdir public
    - cd build/$PHP_BUILD_VERSION_SHORT/$DATABASE_TYPE
    - tar -zcvf ../../../public/php$PHP_BUILD_VERSION_SHORT-ci.tgz docker-compose.yml .env .env.example .env.testing

store:artifacts:
  <<: *deploy-artifact-common
  stage: release
  needs:
    - deploy:artifact:nonginx
    - deploy:artifact:nginx
  script:
    - ls -la public
  artifacts:
    expire_in: 2 yrs
    paths:
      - public/
    when: on_success
