<?php

declare(strict_types=1);

use App\DatabaseType;
use App\Dockercompose\Files\Php;
use App\Dockerfile\Files\Php as PhpDockerfile;
use App\Dockerfile\Templates\Development80;
use App\Dockerfile\Templates\Development;
use App\Dockerfile\Templates\Pgsql14;
use App\Dockerfile\Templates\Pgsql17;
use App\Dockerfile\Templates\PhpOsInstall;
use App\Dockerfile\Templates\Php80OsInstall;
use App\Envfiles\Files\Env;

require_once __DIR__ . '/vendor/autoload.php';

$exportDirectory = __DIR__ . '/build/' . getenv('PHP_BUILD_VERSION_SHORT') . '/pgsql/';
$databaseType = new DatabaseType(DatabaseType::TYPE_PGSQL);

$composeFile = new Php();
$composeFile->run(
    $exportDirectory,
    'docker-compose.yml',
    $databaseType,
);

// 8.0 is built on a lower version of Alpine, so it has worse requirements
$osInstallClass = new PhpOsInstall();
$developmentInstallClass = new Development();
$pgsqlInstallClass = new Pgsql17();
if (str_starts_with(getenv('PHP_BUILD_VERSION'), '8.0')) {
    $osInstallClass = new Php80OsInstall();
    $developmentInstallClass = new Development80();
    $pgsqlInstallClass = new PgSql14();
}
$dockerfile = new PhpDockerfile($osInstallClass, $developmentInstallClass, $pgsqlInstallClass);
$dockerfile->run(
    $exportDirectory,
    'Dockerfile',
);

$envExample = new Env(
    $exportDirectory,
    $databaseType,
    '.env.example',
);
$envExample->run();

$envTesting = new Env(
    $exportDirectory,
    $databaseType,
    '.env.testing',
    true,
);
$envTesting->run();

$envMain = new Env(
    $exportDirectory,
    $databaseType,
    '.env',
);
$envMain->run();
