<?php

declare(strict_types=1);

require_once __DIR__ . '/CompareIniFiles.php';

$result = (new CompareIniFiles())->execute();

// If we have any result, we had some errors.
if ($result === []) {
    // No errors. Exit safely
    exit(0);
}

// Write to stderr so the calling shell script exits properly
fwrite(STDERR, 'Errors discovered. Exiting' . PHP_EOL . PHP_EOL);

// echo each error to stderr
foreach ($result as $errorString) {
    fwrite(STDERR, $errorString . PHP_EOL . PHP_EOL);
}

// Always exit 1 to ensure scripts stop safely
exit(1);
