#!/bin/sh

# Exit the script if there is an error
set -e

# switch to compare the values in the projects .env files
if [ "${COMPARE_ENV_FILES:-0}" -eq "1" ]; then
  php /usr/local/bin/file-compare.php
fi

# If not arguments are supplied, start php-fpm in background mode
if [ $# -eq 0 ]; then
  exec php-fpm -F -R
fi

# Now running any arbitrary commands that might have been passed in
exec "$@"
