<?php

declare(strict_types=1);

/**
 * Class CheckFiles
 *
 * This class checks the provided .env files (if they exist) and ensures they have the same keys
 *
 * This should be called from a shall script with `set -e` for proper behaviour i.e. stop on error
 */
class CompareIniFiles
{
    /**
     * Static key used to allow nice messages in the output
     */
    private const string ARRAY_KEY_FOR_FILENAME = 'CHECK_FILES_FILENAME';

    private string $directoryToCheck = '';

    private array $filesToCheck = [];

    /**
     * CheckFiles constructor.
     *
     * @param  string|null  $defaultDirectory  absolute path to directory containing your .env files. Null means __DIR__
     * @param  array  $extraFilesToCheck  An array of ini files, relative to defaultDirectory, to compare
     */
    public function __construct(?string $defaultDirectory = '/var/www/html', array $extraFilesToCheck = [])
    {
        // Ensure we always have a default directory
        if ($defaultDirectory === null || $defaultDirectory === '' || $defaultDirectory === '0') {
            $defaultDirectory = __DIR__;
        }

        // Set up the default directory and files
        $this->setDefaultDirectory($defaultDirectory)
            ->getAllEnvFiles();

        // Add any extra files that we want to check
        foreach ($extraFilesToCheck as $extraFileToCheck) {
            $this->addExtraFile($this->directoryToCheck . $extraFileToCheck);
        }
    }

    /**
     * Checks that keys match in the .env files
     *
     * @return string[]
     */
    public function execute(): array
    {
        $result = [];

        // Loop through the array of ini files to check, and compare them all against each other
        $filesCount = count($this->filesToCheck);
        for ($i = 0; $i < $filesCount; $i++) {
            // Check forward if we can
            $localIndex = $i;
            while (++$localIndex < $filesCount) {
                $result[] = $this->checkArrays(
                    $this->getIniFile($this->filesToCheck[$i]),
                    $this->getIniFile($this->filesToCheck[$localIndex]),
                );
            }

            // Check back if we can
            $localIndex = $i;
            while (--$localIndex >= 0) {
                $result[] = $this->checkArrays(
                    $this->getIniFile($this->filesToCheck[$i]),
                    $this->getIniFile($this->filesToCheck[$localIndex]),
                );
            }
        }

        // Clean out empty values
        return array_filter($result);
    }

    /**
     * Finds all the .env* files in the default directory
     */
    private function getAllEnvFiles(): void
    {
        foreach (glob($this->directoryToCheck . '.env*') as $file) {
            $this->addExtraFile($file);
        }
    }

    private function addExtraFile(string $fileToAdd): void
    {
        // If the file doesn't exist, return early
        if (!file_exists($fileToAdd)) {
            return;
        }
        $this->filesToCheck[] = $fileToAdd;
    }

    private function getIniFile(string $fileName): array
    {
        // Convert the file to an array
        $fileAsArray = $this->myParseIniFile($fileName);
        if ($fileAsArray === [] || $fileAsArray === false) {
            return [];
        }
        // Add the filename
        $fileAsArray[self::ARRAY_KEY_FOR_FILENAME] = basename($fileName);
        return $fileAsArray;
    }

    private function checkArrays(array $first, array $second): ?string
    {
        $result = null;
        $arrayDifference = array_diff_key($first, $second);
        if ($arrayDifference !== []) {
            $result = "{$first[self::ARRAY_KEY_FOR_FILENAME]} has keys that {$second[self::ARRAY_KEY_FOR_FILENAME]} does not." . PHP_EOL;
            $result .= 'Offending keys: ' . implode(', ', array_keys($arrayDifference));
        }
        return $result;
    }

    /**
     * @return $this
     */
    private function setDefaultDirectory(string $directoryToSet): self
    {
        // Make sure we always have the directory separator
        if (substr($directoryToSet, -1) !== DIRECTORY_SEPARATOR) {
            $directoryToSet .= DIRECTORY_SEPARATOR;
        }
        $this->directoryToCheck = $directoryToSet;
        return $this;
    }

    /**
     * Remove bash comments from an ini file
     *
     * parse_ini_file deprecated support for comments starting with a hash in PHP 7.0
     *
     * As we're not using the dotenv package, these need to be removed manually
     *
     *
     * @return array|false
     */
    private function myParseIniFile(string $file): array|false
    {
        return parse_ini_string(
            (string) preg_replace('/^#.*\\n/m', '', @file_get_contents($file)),
            false,
            INI_SCANNER_RAW,
        );
    }
}

//(new CheckFiles())->execute();
