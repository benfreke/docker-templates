<?php

declare(strict_types=1);

use App\DatabaseType;
use App\Dockercompose\Files\Php;
use App\Dockerfile\Files\Php as PhpDockerfile;
use App\Dockerfile\Templates\Development;
use App\Dockerfile\Templates\Development80;
use App\Dockerfile\Templates\Mysql;
use App\Dockerfile\Templates\PhpOsInstall;
use App\Dockerfile\Templates\Php80OsInstall;
use App\Envfiles\Files\Env;

require_once __DIR__ . '/vendor/autoload.php';

$exportDirectory = __DIR__ . '/build/' . getenv('PHP_BUILD_VERSION_SHORT') . '/mysql/';
$databaseType = new DatabaseType(DatabaseType::TYPE_MYSQL);

$composeFile = new Php();
$composeFile->run(
    $exportDirectory,
    'docker-compose.yml',
    $databaseType,
);

// 8.0 is built on a lower version of Alpine, so it has worse requirements
$osInstallClass = new PhpOsInstall();
$developmentInstallClass = new Development();
if (str_starts_with(getenv('PHP_BUILD_VERSION'), '8.0')) {
    $osInstallClass = new Php80OsInstall();
    $developmentInstallClass = new Development80();
}
$dockerfile = new PhpDockerfile($osInstallClass, $developmentInstallClass, new Mysql());
$dockerfile->run(
    $exportDirectory,
    'Dockerfile',
);

$envExample = new Env(
    $exportDirectory,
    $databaseType,
    '.env.example',
);
$envExample->run();

$envTesting = new Env(
    $exportDirectory,
    $databaseType,
    '.env.testing',
    true,
);
$envTesting->run();

$envMain = new Env(
    $exportDirectory,
    $databaseType,
    '.env',
);
$envMain->run();
